<%-- 
    Document   : frmActualizarConcepto
    Created on : 10/03/2018, 07:51:41 AM
    Author     : AMBIENTE01
--%>

<%@page import="Encapsulamiento.Concepto"%>
<%@page import="EJB.EJBConcepto"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Actualizar Concepto</title>
        <%
        EJBConcepto ejbconcepto = new EJBConcepto();
        
        int IdConcepto = Integer.parseInt(request.getParameter("Id"));
        
        Concepto c = ejbconcepto.buscar(IdConcepto);
        %>
    </head>
    <body>
        <h1>Actualizar Concepto</h1>
        <form action="ServletUpdate" method="post">
            <input type="hidden" name="tabla" value="concepto">
            <input type="hidden" name="IdConcepto" value="<%=c.getIdConcepto() %>">
            <label for="nombre">Nombre</label>
            <input type="text" name="nombre" value="<%=c.getNombre()%>" id="nombre">
            <label for="descripcion">Nombre</label>
            <input type="text" name="descripcion" value="<%=c.getDescripcion()%>" id="descripcion">
             <button type="submit">Actualizar</button>
        </form>
    </body>
</html>
