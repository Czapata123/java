<%-- 
    Document   : frmInsertGasto
    Created on : 6/08/2018, 08:54:22 AM
    Author     : AMBIENTE01
--%>

<%@page import="Encapsulamiento.Gasto"%>
<%@page import="EJB.EJBGasto"%>
<%@page import="Encapsulamiento.Concepto"%>
<%@page import="EJB.EJBConcepto"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type="text/css" href="css/estilos.css">
        <title>Guardar Gasto</title>
        <%
            EJBConcepto ejbConcepto = new EJBConcepto();
            ejbConcepto.crearLista();

            EJBGasto ejbgasto = new EJBGasto();
            ejbgasto.crearLista();

        %>
    </head>
    <body>
        <h1>Insertar Gasto</h1>
        <form action="ServletInsert" method="post">
            <input type="hidden" name="tabla" value="gasto">
            <select name="concepto">
                <%                      
                    for (Concepto c : ejbConcepto.getListaConcepto()) {
                %>
                <option value="<%=c.getIdConcepto()%>">
                    <%=c.getNombre()%></option>
                    <%}%>
            </select>
            <label for="valor">Valor</label>
            <input type="text" name="valor" placeholder="valor gastado">
            <label for="descripcion"></label>
            <input type="text" name="descripcion" placeholder="descripcion de gasto">
            <button type="submit">Guardar</button>  
        </form>
        <h1>Lista de Gastos</h1>
        <table class="table">
            <tr>
                <td>Id</td>
                <td>Concepto</td>
                <td>Valor</td>
                <td>Fecha</td>
                <td>Descripcion</td>
            </tr>
            <%
                for (Gasto g : ejbgasto.getListaGasto()) {
            %>
            <tr>
                <td><%=g.getIdGasto()%></td>

                <td><%=g.getConcepto().getNombre()%></td>

                <td><%=g.getValor()%></td>

                <td><%=g.getFecha()%></td>

                <td><%=g.getDescripcion()%></td>

                <td><a href="frmActualizarGasto.jsp?Id=<%=g.getIdGasto()%>">Actualizar</a></td>

                <td><a onclick="return confirm('¿Esta Seguro?');" href="ServletDelete?Id=<%=g.getIdGasto()%>&xw1='1x'" id="delete">Eliminar</a>
                </td>
            </tr>
            <%}%>

        </table>

        <h1>Insertar Concepto</h1>
        <form action="ServletInsert" method="post">
            <input type="hidden" name="tabla" value="concepto">
            <label for="nombreConcepto">Nombre</label>
            <input type="text" name="nombre" id="nombreConcepto">
            <label for="descripcionConcepto">Descripcion</label>
            <input type="text" name="descripcion" id="descripcionConcepto">
            <button type="submit">Guardar</button>
        </form>
        <h1>Lista de Conceptos</h1>
        <table class="table">
            <tr>
                <td>Id</td>
                <td>Nombre</td>
                <td>Descripcion</td>
            </tr>
            <%
                for (Concepto c : ejbConcepto.getListaConcepto()) {
            %>
            <tr>
                <td><%=c.getIdConcepto()%></td>

                <td><%=c.getNombre()%></td>

                <td><%=c.getDescripcion()%></td>


                <td><a href="frmActualizarConcepto.jsp?Id=<%=c.getIdConcepto() %>">Actualizar</a></td>

                <td><a onclick="return confirm('¿Esta Seguro?');" href="ServletDelete?Id=<%=c.getIdConcepto()%>&xw1='2x'">Eliminar</a>
                </td>
            </tr>
            <%}%>

        </table>
    </body>
</html>
