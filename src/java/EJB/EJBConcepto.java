/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EJB;

import DAO.DaoConcepto;
import DB_Conexion.Conexion;
import Encapsulamiento.Concepto;
import com.mysql.jdbc.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;

/**
 *
 * @author AMBIENTE01
 */
@Stateless
public class EJBConcepto {

    private Concepto concepto;
    private List<Concepto> listaConcepto;
    private Connection conexion;
    private final DaoConcepto daoConcepto;

//<editor-fold defaultstate="collapsed" desc="Encapsulamiento y Constructor">
    public EJBConcepto() {
        daoConcepto = new DaoConcepto();
        concepto = new Concepto();
        listaConcepto = new ArrayList<>();
    }

    public Concepto getConcepto() {
        return concepto;
    }

    public void setConcepto(Concepto concepto) {
        this.concepto = concepto;
    }

    public List<Concepto> getListaConcepto() {
        return listaConcepto;
    }

    public void setListaConcepto(List<Concepto> listaConcepto) {
        this.listaConcepto = listaConcepto;
    }

    public Connection getConection() {
        return conexion;
    }

    public void setConection(Connection conection) {
        this.conexion = conection;
    }// </editor-fold>

    public void crearLista() throws SQLException {
        //crea una variable de tipo ResultSet
        ResultSet rs;
        try {
            //obtener la conexion de la clase respectiva
            conexion = Conexion.conectar();
            /*
            obtener los datos del select de la tabla
            concepto usando el metodo listar del DAO
             */
            rs = daoConcepto.listar(conexion);
            Concepto c;
            listaConcepto.clear();
            /*
            Recorrer el resultset para emsamblar
            los objetos de la clase concepto
             */
            while (rs.next()) {
                //asignar valor a los atributos del objeto
                //concepto tomando los valores del resultset
                //se crea un objeto nuevo por cada vuelta del ciclo
                c = new Concepto();
                c.setIdConcepto(rs.getInt("c.idConcepto"));
                c.setNombre(rs.getString("c.nombre"));
                c.setDescripcion(rs.getString("c.descripcion"));
                listaConcepto.add(c);
            }
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        } finally {
            conexion.close();
        }
    }

    public Concepto buscar(int id) throws SQLException {
        //validar si el atributo listaConcepto esta vacio
        if (getListaConcepto().isEmpty()) {
            //si esta vacio llamamos el metodo crearlista
            crearLista();
        }
        /*recorer el atributo listaCocepto para buscar
          el objeto correspondiente al id que llego como
        parametro
         */
        for (Concepto c : getListaConcepto()) {
            if (c.getIdConcepto() == id) {
                return c;
            }
        }
        return null;
    }

    public int insert() throws SQLException {
        //Definir las variables para el retorno al insertar
        int valorRetorno;
        try {
            //establecer la conexion
            conexion = Conexion.conectar();
            /**
             * Ejecutar el metodo insert del DAO y guardar el resultado en el
             * valor de retorno
             */
            valorRetorno = daoConcepto.insert(concepto, conexion);
            conexion.close();
        } catch (Exception ex) {
            valorRetorno = 0;
        } finally {
            conexion.close();
        }
        return valorRetorno;
    }

    public boolean update() throws SQLException {
        /**
         * ¿Por que el metodo update lleva parametros y el insert no? no se hace
         * necesario ya que se utilizan los atributos de el EJB.
         */
        //valor de retorno
        boolean valorRetorno;
        try {
            //establecer conexxion
            conexion = Conexion.conectar();
            /*
             *Ejecutar el metodo
             *update y guardar 
             *el valor
             */
            valorRetorno = daoConcepto.update(concepto, conexion);
            conexion.close();
        } catch (Exception ex) {

            valorRetorno = false;

        } finally {
            conexion.close();
        }
        return valorRetorno;
    }

    public boolean delete() throws SQLException {
        boolean valorRetorno;
        try {
            conexion = Conexion.conectar();
            valorRetorno = daoConcepto.delete(concepto, conexion);
            conexion.close();
        } catch (Exception ex) {
            valorRetorno = false;
        } finally {
            conexion.close();
        }
        return valorRetorno;
    }
}
