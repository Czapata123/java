/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EJB;

import DAO.DaoGasto;
import DB_Conexion.Conexion;
import Encapsulamiento.Gasto;
import com.mysql.jdbc.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;

/**
 *
 * @author AMBIENTE01
 */
@Stateless
public class EJBGasto {

    private Gasto gasto;
    private List<Gasto> listaGasto;
    private Connection conexion;
    private final DaoGasto daoGasto;

//<editor-fold defaultstate="collapsed" desc="Encapsulamiento y Constructor">
    public EJBGasto() {
        gasto = new Gasto();
        listaGasto = new ArrayList<>();
        daoGasto = new DaoGasto();
    }

    public Gasto getGasto() {
        return gasto;
    }

    public void setGasto(Gasto gasto) {
        this.gasto = gasto;
    }

    public List<Gasto> getListaGasto() {
        return listaGasto;
    }

    public void setListaGasto(List<Gasto> listaGasto) {
        this.listaGasto = listaGasto;
    }

    public Connection getConexion() {
        return conexion;
    }

    public void setConexion(Connection conexion) {
        this.conexion = conexion;
    }// </editor-fold>

    public void crearLista() throws SQLException {
        //variable de tipo resultset
        ResultSet rs;
        try {
            conexion = Conexion.conectar();
            rs = daoGasto.listar(conexion);
            Gasto g;
            listaGasto.clear();
            EJBConcepto ejbC = new EJBConcepto();
            while (rs.next()) {
                g = new Gasto();
                g.setIdGasto(rs.getInt("g.IdGasto"));
                g.setValor(rs.getFloat("g.Valor"));
                g.setDescripcion(rs.getString("g.descripcion"));
                //g.setConcepto(new EJBConcepto().buscar(rs.getInt("g.idConcepto")));
                g.setConcepto(ejbC.buscar(rs.getInt("g.idConcepto")));
                g.setFecha(rs.getDate("g.fecha"));
                listaGasto.add(g);
            }
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        } finally {
            conexion.close();
        }
    }

    public Gasto buscar(int id) throws SQLException {
        //validar si el atributo listaConcepto esta vacio
        if (getListaGasto().isEmpty()) {
            //si esta vacio llamamos el metodo crearlista
            crearLista();
        }
        /*recorer el atributo listaCocepto para buscar
          el objeto correspondiente al id que llego como
        parametro
         */
        for (Gasto g : getListaGasto()) {
            if (g.getIdGasto() == id) {
                return g;
            }
        }
        return null;
    }

    public int insert() throws SQLException {
        //definir una variable para el retorno al insertar
        int valorRetorno;
        try {
            //establecer la concexion
            conexion = Conexion.conectar();
            /*Ejecutar el metodo insert del DAO y
              guardar el resultado en valorRetorno
             */
            valorRetorno = daoGasto.insert(gasto, conexion);
            conexion.close();
        } catch (Exception ex) {
            valorRetorno = 0;
        } finally {
            conexion.close();
        }
        return valorRetorno;
    }

    public boolean update() throws SQLException {
        boolean valorRetorno;
        try {
            conexion = Conexion.conectar();
            valorRetorno = daoGasto.update(gasto, conexion);
            conexion.close();
        } catch (Exception ex) {
            valorRetorno = false;
        } finally {
            conexion.close();
        }
        return valorRetorno;
    }
        public boolean delete() throws SQLException{
        boolean valorRetorno;
        try{
            conexion = Conexion.conectar();
            valorRetorno = daoGasto.delete(gasto, conexion);
            conexion.close();
        } catch(Exception ex){
            valorRetorno = false;
        } finally{
            conexion.close();
        }
        return valorRetorno;
    }
}
