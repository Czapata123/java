/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import Encapsulamiento.Concepto;
import com.mysql.jdbc.Connection;
import com.mysql.jdbc.Statement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;


/**
 *
 * @author AMBIENTE01
 */
public class DaoConcepto {
    private ResultSet rs;
    private Statement statement;
    private String sql;
    PreparedStatement statementId;
    
    public ResultSet listar(Connection cnx){
        try{
            //asignar null por defecto al ResultSet
            rs = null;
            //definir la consulta de seleccion
            sql = "SELECT c.* FROM concepto c";
            //crear la sentencia sobre la coneccion
            statement = (Statement) cnx.createStatement();
            /*Ejecutar el select y guardar el resultado
            en el ResultSet(rs)
            */
            rs = statement.executeQuery(sql);
         }catch(Exception ex){
            System.out.println(ex.getMessage());
        }
        //retorna ResultSet
        return rs;
    }
    public int insert(Concepto c, Connection cnx) throws SQLException{
        statement = (Statement) cnx.createStatement();
        
        sql = "INSERT INTO Concepto(nombre, descripcion)"
              + "VALUES ('"+ c.getNombre() +"',"
              + "'" + c.getDescripcion() + "')";
        //statement.executeQuery(sql);
        //Retornar el Id Autogenerado
        statementId = (PreparedStatement) cnx.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
        int filasAfectadas = statementId.executeUpdate();
        ResultSet ids = statementId.getGeneratedKeys();
        int idGenerado = 0;
        if(ids.next()){
            idGenerado = ids.getInt(1);
        }
        return idGenerado;
    }
    public boolean update(Concepto c, Connection cnx) throws SQLException{
        statement = (Statement) cnx.createStatement();
        sql =   "UPDATE concepto " +
                "SET nombre = '" + c.getNombre() + "'," 
              + "descripcion = '" + c.getDescripcion() + "'"
              + "where idConcepto =" + c.getIdConcepto();
        statement.execute(sql);
        return true;
    }
    public boolean delete(Concepto c,Connection cnx) throws SQLException{
        statement = (Statement) cnx.createStatement();
        sql =   "DELETE FROM concepto WHERE idConcepto = " + c.getIdConcepto() + ";";
        statement.execute(sql);
        return true;
    }
}
