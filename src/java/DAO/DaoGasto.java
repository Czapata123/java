/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import Encapsulamiento.Gasto;
import com.mysql.jdbc.Connection;
import com.mysql.jdbc.Statement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author AMBIENTE01
 */
public class DaoGasto {
    private ResultSet rs;
    private Statement statement;
    private String sql;
    PreparedStatement statementId;
    
    
    public ResultSet listar(Connection cnx){
        try{
            //asignar null por defecto al ResultSet
            rs = null;
            //definir la consulta de seleccion
            sql = "SELECT g.* FROM gasto g";
            //crear la sentencia sobre la coneccion
            statement = (Statement) cnx.createStatement();
            /*Ejecutar el select y guardar el resultado
            en el ResultSet(rs)
            */
            rs = statement.executeQuery(sql);
         }catch(Exception ex){
            System.out.println(ex.getMessage());
        }
        //retorna ResultSet
        return rs;
    }
    
    public int insert(Gasto g,Connection cnx) throws SQLException{
        statement = (Statement) cnx.createStatement();
        
        sql = "INSERT INTO gasto(idConcepto,valor,fecha,descripcion) "
            + "VALUES (" + g.getConcepto().getIdConcepto() + ","
            + g.getValor() + ",CURDATE(),'" + g.getDescripcion() + "');";
        //statement.executeQuery(sql);
        //retornar id autogenerado
        statementId = (PreparedStatement) cnx.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
        int filasAfectadas = statementId.executeUpdate();
        ResultSet ids = statementId.getGeneratedKeys();
        int idGenerado = 0;
        if(ids.next()){
            idGenerado = ids.getInt(1);
        }
        return idGenerado;
    }
    public boolean update(Gasto g,Connection cnx) throws SQLException{
        statement = (Statement) cnx.createStatement();
        
        sql = "UPDATE gasto " 
              + "SET idConcepto = "+ g.getConcepto().getIdConcepto() +","
              + "valor = "+ g.getValor() + ","
              + "fecha =' " + g.getFecha() + "',"
              + "descripcion = '" + g.getDescripcion() + "'" 
              + "WHERE idGasto = " + g.getIdGasto();
        statement.execute(sql);
        return true;
    }
    public boolean delete (Gasto g, Connection cnx) throws SQLException{
         statement = (Statement) cnx.createStatement();
        sql = "DELETE FROM gasto WHERE idGasto = " + g.getIdGasto();
        statement.execute(sql);
        return true;      
    }
}
