/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Encapsulamiento;

import java.sql.Date;

/**
 *
 * @author AMBIENTE01
 */
public class Gasto {
    private int idGasto;
    private Concepto concepto;
    private float valor;
    private Date fecha;
    private String descripcion;
    
    /*
    cuando tenemos  asociacion en 
    un atributo de la clase
    en el constructor se debe instaciar 
    la clase asociada al atributo
    */
    public Gasto(){
        concepto = new Concepto();//instancia de la asociacion
    }
    //metodos get y set

    public int getIdGasto() {
        return idGasto;
    }

    public void setIdGasto(int idGasto) {
        this.idGasto = idGasto;
    }

    public Concepto getConcepto() {
        return concepto;
    }

    public void setConcepto(Concepto concepto) {
        this.concepto = concepto;
    }

    public float getValor() {
        return valor;
    }

    public void setValor(float valor) {
        this.valor = valor;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
    
 
}
