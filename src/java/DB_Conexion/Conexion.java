/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DB_Conexion;

import com.mysql.jdbc.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author AMBIENTE01
 */
public class Conexion {
    public static Connection conectar(){
        Connection conexion = null;
        try{
            Class.forName("com.mysql.jdbc.Driver");
            String servidor = "jdbc:mysql://localhost/controlgastos";
            String usuario = "root";
            String contraseña = "";
            conexion = (Connection) DriverManager.getConnection(servidor, usuario, contraseña);
        }catch(ClassNotFoundException | SQLException ex){
            System.out.println(ex.getMessage());
        }finally{
            return conexion;
        }
    }
}
