/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlet;

import EJB.EJBConcepto;
import EJB.EJBGasto;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author AMBIENTE01
 */
@WebServlet(name = "ServletDelete", urlPatterns = {"/ServletDelete"})
public class ServletDelete extends HttpServlet {

    EJBGasto ejbgasto = new EJBGasto();
    EJBConcepto ejbconcepto = new EJBConcepto();

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try {
            String mensajeRespuesta = "";
            if (request.getMethod().equals("GET")) {

                if (request.getParameter("xw1").equals("'1x'")) {
                    ejbgasto.getGasto().
                            setIdGasto(Integer.
                                    parseInt(request.getParameter("Id")));
                    if (ejbgasto.delete() == true) {
                        mensajeRespuesta = "El registro ha sido Eliminado";
                    } else {
                        mensajeRespuesta = "No se ha podio eliminar el registro";
                    }
                }

                if (request.getParameter("xw1").equals("'2x'")) {
                    ejbconcepto.getConcepto().
                            setIdConcepto(
                                    Integer.parseInt(
                                            request.getParameter("Id")));
                    if (ejbconcepto.delete() == true) {
                        mensajeRespuesta = "El registro ha sido Eliminado";
                    } else {
                        mensajeRespuesta = "No se ha podio eliminar el registro";
                    }
                }

                PrintWriter out = response.getWriter();
                out.println("<!DOCTYPE html>");
                out.println("<html>");
                out.println("<head>");
                out.println("<title>Servlet ServletDelete</title>");
                out.println("</head>");
                out.println("<body>");
                out.println("<h1>" + mensajeRespuesta + "</h1>");
                out.println("<a href=\"frmGastosConceptos.jsp\">volver</a>");
                out.println("</body>");
                out.println("</html>");
            }
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
