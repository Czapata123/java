/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlet;

import EJB.EJBConcepto;
import EJB.EJBGasto;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Date;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author AMBIENTE01
 */
@WebServlet(name = "ServletUpdate", urlPatterns = {"/ServletUpdate"})
public class ServletUpdate extends HttpServlet {

    private EJBGasto ejbgasto = new EJBGasto();
    private EJBConcepto ejbconcepto = new EJBConcepto();

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try {
            String mensajeRespuesta = "";

            if (request.getMethod().equals("POST")) {
                if (request.getParameter("tabla").equals("gasto")) {
                    ejbgasto.getGasto().setIdGasto(
                            Integer.parseInt(
                                    request.getParameter("IdGasto")));
                    ejbgasto.getGasto().
                            setConcepto(new EJBConcepto().
                                    buscar(Integer.parseInt(
                                            request.getParameter("concepto"))));

                    ejbgasto.getGasto().
                            setValor(Float.parseFloat(
                                    request.getParameter("valor")));

                    ejbgasto.getGasto().setFecha(Date.valueOf(request.getParameter("fecha")));

                    ejbgasto.getGasto().
                            setDescripcion(
                                    request.getParameter("descripcion"));

                    if (ejbgasto.update() == true) {
                        mensajeRespuesta = "Se ha Actualizado el Registro";
                    } else {
                        mensajeRespuesta = "Actualizacion Fallida";
                    }
                }
                if (request.getParameter("tabla").equals("concepto")) {
                    ejbconcepto.getConcepto().setIdConcepto(Integer.parseInt(
                            request.getParameter("IdConcepto")));
                    ejbconcepto.getConcepto().setNombre(request.getParameter("nombre"));
                    ejbconcepto.getConcepto().setDescripcion(request.getParameter("descripcion"));

                    if (ejbconcepto.update() == true) {
                        mensajeRespuesta = "Se ha Actualizado el Registro";
                    } else {
                        mensajeRespuesta = "Actualizacion Fallida";
                    }
                }

                PrintWriter out = response.getWriter();
                out.println("<!DOCTYPE html>");
                out.println("<html>");
                out.println("<head>");
                out.println("<h1>" + mensajeRespuesta + "</h1>");
                out.println("</head>");
                out.println("<body>");
                out.println("<h1>" + "</h1>");
                out.println("<a href=\"frmGastosConceptos.jsp\">volver</a>");
                out.println("</body>");
                out.println("</html>");
            }
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
