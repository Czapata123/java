/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlet;

import EJB.EJBConcepto;
import EJB.EJBGasto;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author AMBIENTE01
 */
@WebServlet(name = "ServletInsert", urlPatterns = {"/ServletInsert"})
public class ServletInsert extends HttpServlet {
    //se crea un objeto
    private EJBGasto ejbGasto= new EJBGasto();
    private EJBConcepto ejbconcepto = new EJBConcepto();
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try{
            if(request.getMethod().equals("POST")){
                
                String tabla = request.getParameter("tabla");
                String mensajeRespuesta = "";
                if(tabla.equals("gasto")){
                ejbGasto.getGasto().
                        setConcepto(new EJBConcepto().
                        buscar(Integer.parseInt(
                        request.getParameter("concepto"))));

                ejbGasto.getGasto().
                        setValor(Float.parseFloat(
                        request.getParameter("valor")));

                ejbGasto.getGasto().
                            setDescripcion(
                            request.getParameter("descripcion"));

                
                int idGenerado = ejbGasto.insert();
                if(idGenerado > 0){
                    mensajeRespuesta = "Insercion Correcta";
                } else {
                    mensajeRespuesta = "Error al insertar gasto";
                }
                } else if(tabla.equals("concepto")){
                    ejbconcepto.getConcepto().setNombre(request.getParameter("nombre"));
                    ejbconcepto.getConcepto().setDescripcion(request.getParameter("descripcion"));
                    int IdGeneradoC = ejbconcepto.insert();
                    if(IdGeneradoC > 0){
                         mensajeRespuesta = "Insercion Correcta";
                     } else {
                         mensajeRespuesta = "Error";
                    }
                }
            PrintWriter out =  response.getWriter();
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ServletInsert</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>" + mensajeRespuesta + "</h1>");
            out.println("<a href=\"frmGastosConceptos.jsp\">volver</a>");
            out.println("</body>");
            out.println("</html>");
        }
        }catch(Exception ex){
            System.out.println(ex.getMessage());
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
