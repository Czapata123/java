<%-- 
    Document   : frmActualizarGasto
    Created on : 10/08/2018, 11:39:07 AM
    Author     : AMBIENTE01
--%>

<%@page import="Encapsulamiento.Gasto"%>
<%@page import="EJB.EJBConcepto"%>
<%@page import="Encapsulamiento.Concepto"%>
<%@page import="EJB.EJBGasto"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Actualizar Gasto</title>
        <%
        EJBGasto ejbgasto = new EJBGasto();
        EJBConcepto ejbConcepto= new EJBConcepto();
        ejbConcepto.crearLista();
        
        int IdGasto = Integer.parseInt(request.getParameter("Id"));
        Gasto g = ejbgasto.buscar(IdGasto);
        %>
    </head>
    <body>
        <h1>Actulizar Gasto</h1>
        <form action="ServletUpdate" method="post">
             <input type="hidden" name="tabla" value="gasto">
            <select name="concepto">
            <%
            for (Concepto c : ejbConcepto.getListaConcepto()){
                if(c.getIdConcepto() == g.getConcepto().getIdConcepto()){
            %>
            
            <option selected="true" value="<%=c.getIdConcepto()%>">
            <%=c.getNombre()%></option>
            <%}else{%>           
            <option value="<%=c.getIdConcepto()%>">
            <%=c.getNombre()%></option>
            
            <%}
            }
            %>
            </select>
            <input type="hidden" name="IdGasto" value="<%=g.getIdGasto()%>">
            <label for="valor">valor</label>
            <input type="text" name="valor" placeholder="valor gastado" value="<%=g.getValor()%>">
            <label for="fecha">fecha</label>
            <input type="date" name="fecha" value="<%=g.getFecha()%>">
            <label for="descripcion">Descripcion</label>
            <input type="text" name="descripcion" placeholder="descripcion de gasto" value="<%=g.getDescripcion()%>">
            <button type="submit">Actualizar</button>
        </form>
    </body>
</html>
