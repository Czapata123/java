package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import Encapsulamiento.Gasto;
import EJB.EJBGasto;
import Encapsulamiento.Concepto;
import EJB.EJBConcepto;

public final class frmGastosConceptos_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("<!DOCTYPE html>\n");
      out.write("<html>\n");
      out.write("    <head>\n");
      out.write("        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n");
      out.write("        <link rel=\"stylesheet\" type=\"text/css\" href=\"css/estilos.css\">\n");
      out.write("        <title>Guardar Gasto</title>\n");
      out.write("        ");

            EJBConcepto ejbConcepto = new EJBConcepto();
            ejbConcepto.crearLista();

            EJBGasto ejbgasto = new EJBGasto();
            ejbgasto.crearLista();

        
      out.write("\n");
      out.write("    </head>\n");
      out.write("    <body>\n");
      out.write("        <h1>Insertar Gasto</h1>\n");
      out.write("        <form action=\"ServletInsert\" method=\"post\">\n");
      out.write("            <input type=\"hidden\" name=\"tabla\" value=\"gasto\">\n");
      out.write("            <select name=\"concepto\">\n");
      out.write("                ");
                      
                    for (Concepto c : ejbConcepto.getListaConcepto()) {
                
      out.write("\n");
      out.write("                <option value=\"");
      out.print(c.getIdConcepto());
      out.write("\">\n");
      out.write("                    ");
      out.print(c.getNombre());
      out.write("</option>\n");
      out.write("                    ");
}
      out.write("\n");
      out.write("            </select>\n");
      out.write("            <label for=\"valor\">Valor</label>\n");
      out.write("            <input type=\"text\" name=\"valor\" placeholder=\"valor gastado\">\n");
      out.write("            <label for=\"descripcion\"></label>\n");
      out.write("            <input type=\"text\" name=\"descripcion\" placeholder=\"descripcion de gasto\">\n");
      out.write("            <button type=\"submit\">Guardar</button>  \n");
      out.write("        </form>\n");
      out.write("        <h1>Lista de Gastos</h1>\n");
      out.write("        <table class=\"table\">\n");
      out.write("            <tr>\n");
      out.write("                <td>Id</td>\n");
      out.write("                <td>Concepto</td>\n");
      out.write("                <td>Valor</td>\n");
      out.write("                <td>Fecha</td>\n");
      out.write("                <td>Descripcion</td>\n");
      out.write("            </tr>\n");
      out.write("            ");

                for (Gasto g : ejbgasto.getListaGasto()) {
            
      out.write("\n");
      out.write("            <tr>\n");
      out.write("                <td>");
      out.print(g.getIdGasto());
      out.write("</td>\n");
      out.write("\n");
      out.write("                <td>");
      out.print(g.getConcepto().getNombre());
      out.write("</td>\n");
      out.write("\n");
      out.write("                <td>");
      out.print(g.getValor());
      out.write("</td>\n");
      out.write("\n");
      out.write("                <td>");
      out.print(g.getFecha());
      out.write("</td>\n");
      out.write("\n");
      out.write("                <td>");
      out.print(g.getDescripcion());
      out.write("</td>\n");
      out.write("\n");
      out.write("                <td><a href=\"frmActualizarGasto.jsp?Id=");
      out.print(g.getIdGasto());
      out.write("\">Actualizar</a></td>\n");
      out.write("\n");
      out.write("                <td><a onclick=\"return confirm('¿Esta Seguro?');\" href=\"ServletDelete?Id=");
      out.print(g.getIdGasto());
      out.write("&xw1='1x'\" id=\"delete\">Eliminar</a>\n");
      out.write("                </td>\n");
      out.write("            </tr>\n");
      out.write("            ");
}
      out.write("\n");
      out.write("\n");
      out.write("        </table>\n");
      out.write("\n");
      out.write("        <h1>Insertar Concepto</h1>\n");
      out.write("        <form action=\"ServletInsert\" method=\"post\">\n");
      out.write("            <input type=\"hidden\" name=\"tabla\" value=\"concepto\">\n");
      out.write("            <label for=\"nombreConcepto\">Nombre</label>\n");
      out.write("            <input type=\"text\" name=\"nombre\" id=\"nombreConcepto\">\n");
      out.write("            <label for=\"descripcionConcepto\">Descripcion</label>\n");
      out.write("            <input type=\"text\" name=\"descripcion\" id=\"descripcionConcepto\">\n");
      out.write("            <button type=\"submit\">Guardar</button>\n");
      out.write("        </form>\n");
      out.write("        <h1>Lista de Conceptos</h1>\n");
      out.write("        <table class=\"table\">\n");
      out.write("            <tr>\n");
      out.write("                <td>Id</td>\n");
      out.write("                <td>Nombre</td>\n");
      out.write("                <td>Descripcion</td>\n");
      out.write("            </tr>\n");
      out.write("            ");

                for (Concepto c : ejbConcepto.getListaConcepto()) {
            
      out.write("\n");
      out.write("            <tr>\n");
      out.write("                <td>");
      out.print(c.getIdConcepto());
      out.write("</td>\n");
      out.write("\n");
      out.write("                <td>");
      out.print(c.getNombre());
      out.write("</td>\n");
      out.write("\n");
      out.write("                <td>");
      out.print(c.getDescripcion());
      out.write("</td>\n");
      out.write("\n");
      out.write("\n");
      out.write("                <td><a href=\"frmActualizarConcepto.jsp?Id=");
      out.print(c.getIdConcepto() );
      out.write("\">Actualizar</a></td>\n");
      out.write("\n");
      out.write("                <td><a onclick=\"return confirm('¿Esta Seguro?');\" href=\"ServletDelete?Id=");
      out.print(c.getIdConcepto());
      out.write("&xw1='2x'\">Eliminar</a>\n");
      out.write("                </td>\n");
      out.write("            </tr>\n");
      out.write("            ");
}
      out.write("\n");
      out.write("\n");
      out.write("        </table>\n");
      out.write("    </body>\n");
      out.write("</html>\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
