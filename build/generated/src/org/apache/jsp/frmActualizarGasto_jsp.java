package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import Encapsulamiento.Gasto;
import EJB.EJBConcepto;
import Encapsulamiento.Concepto;
import EJB.EJBGasto;

public final class frmActualizarGasto_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("<!DOCTYPE html>\n");
      out.write("<html>\n");
      out.write("    <head>\n");
      out.write("        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n");
      out.write("        <title>Actualizar Gasto</title>\n");
      out.write("        ");

        EJBGasto ejbgasto = new EJBGasto();
        EJBConcepto ejbConcepto= new EJBConcepto();
        ejbConcepto.crearLista();
        
        int IdGasto = Integer.parseInt(request.getParameter("IdGasto"));
        Gasto g = ejbgasto.buscar(IdGasto);
        
      out.write("\n");
      out.write("    </head>\n");
      out.write("    <body>\n");
      out.write("        <h1>Actulizar Gasto</h1>\n");
      out.write("        <form action=\"ServletUpdate\" method=\"post\">\n");
      out.write("            <select name=\"concepto\">\n");
      out.write("            ");

            for (Concepto c : ejbConcepto.getListaConcepto()){
                if(c.getIdConcepto() == g.getConcepto().getIdConcepto()){
            
      out.write("\n");
      out.write("            \n");
      out.write("            <option selected=\"true\" value=\"");
      out.print(c.getIdConcepto());
      out.write("\">\n");
      out.write("            ");
      out.print(c.getNombre());
      out.write("</option>\n");
      out.write("            ");
}else{
      out.write("           \n");
      out.write("            <option value=\"");
      out.print(c.getIdConcepto());
      out.write("\">\n");
      out.write("            ");
      out.print(c.getNombre());
      out.write("</option>\n");
      out.write("            \n");
      out.write("            ");
}
            }
            
      out.write("\n");
      out.write("            </select>\n");
      out.write("            <input type=\"hidden\" name=\"IdGasto\" value=\"");
      out.print(g.getIdGasto());
      out.write("\">\n");
      out.write("            <label for=\"valor\">valor</label>\n");
      out.write("            <input type=\"text\" name=\"valor\" placeholder=\"valor gastado\" value=\"");
      out.print(g.getValor());
      out.write("\">\n");
      out.write("            <label for=\"fecha\">fecha</label>\n");
      out.write("            <input type=\"date\" name=\"fecha\" value=\"");
      out.print(g.getFecha());
      out.write("\">\n");
      out.write("            <label for=\"descripcion\">Descripcion</label>\n");
      out.write("            <input type=\"text\" name=\"descripcion\" placeholder=\"descripcion de gasto\" value=\"");
      out.print(g.getDescripcion());
      out.write("\">\n");
      out.write("            <button type=\"submit\">Actualizar</button>\n");
      out.write("        </form>\n");
      out.write("    </body>\n");
      out.write("</html>\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
